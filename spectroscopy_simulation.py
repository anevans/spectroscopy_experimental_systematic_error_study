import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy.optimize import minimize
import multiprocessing as mp
from functools import partial
import matplotlib.pyplot as plt
from tqdm import tqdm


class spectroscopy_simulation:
    def __init__(self, freqs= [-1,0,1], nsets= 80, probscaling= 0.01, N0=500, seed= 1, shuffle_order= False, usedepletion= True):
        #freqs - list of frequncy positions to be sampled
        #nsets - the number of times the frequency order is repeated
        #probscaling - an artificial factor multiplied by the PDF to represent the laser power
        #No - number of antihydrogen in the simulation
        #seed - random number seed used
        #shuffle_order - if true, shuffles the frequenct sampling order
        #usedepletion - if true, decreases sample size for each sucess
        self.freqs=freqs
        self.nfreq=len(freqs)
        self.events =  np.zeros((nsets,self.nfreq), dtype = int)
        self.probscaling=probscaling
        self.seed=seed
        self.nsets=nsets
        self.usedepletion=usedepletion
        self.N0=N0

        self.order = list(range(0,self.nfreq)) + list(range(self.nfreq-1,-1,-1))
        if shuffle_order:
            temp= range(0, self.nfreq)
            np.random.shuffle(temp)
            self.order = np.concatenate((temp, np.flip(temp)))

    def Run_simulation(self):
        #the actual experiment 
        N0=self.N0
        np.random.seed(self.seed)
        PDF = norm.pdf
        for ii in range(self.nsets):
            for jj in self.order:
                eventprob = PDF(self.freqs[jj])*self.probscaling
                prob = np.random.rand(N0)
                nevents = sum(prob < eventprob)
                #print("set number: {}\tfreq number: {}\tprob: {}\tnevents: {}".format(ii, jj, eventprob, nevents))
                if N0 - nevents < 0:
                    self.events[ii][jj]+=N0
                    if self.usedepletion:
                        N0=0
                else:
                    self.events[ii][jj]+=nevents
                    if self.usedepletion:
                        N0=N0-nevents
    
    def Calculate_gaussian_fit_parameters(self):
        spectrum=sum(self.events)
        try: 
            self.fit_info, self.cov= curve_fit(lambda x, mu, sig, A : A*np.exp((-1./2)*((x-mu)**2)/(sig**2)), self.freqs, spectrum, p0=[1,1, 150],  sigma= np.sqrt([max(i,1) for i in spectrum]))
        except RuntimeError:
            print("fit error")
            self.fit_info=[9999999, 9999999, 9999999]
            self.cov=[[99999999, 0, 0], [0, 9999999, 0], [0, 0, 9999999]]
        #print(self.fit_info)

def single_run_error( seed=1, freqs=[-4,-3,-2,-1,0,1,2,3,4], usedepletion=False):
    temp=spectroscopy_simulation(seed=seed, freqs=freqs, usedepletion=usedepletion)
    temp.Run_simulation()
    temp.Calculate_gaussian_fit_parameters()
    return((1-temp.fit_info[1])**2)

def calc_mean_variance_offset_squared(freqs=[-4,-3,-2,-1,0,1,2,3,4], trials=1000 ):
    pool = mp.Pool(mp.cpu_count())
    output=pool.map(partial(single_run_error, freqs=freqs, usedepletion=False), range(trials)) 
    pool.close()
    pool.join()
    print(freqs)
    print(sum(output))     
    return(sum(output))

def single_run_fit(seed=1, freqs=[-4,-3,-2,-1,0,1,2,3,4], usedepletion=False):
    temp=spectroscopy_simulation(seed=seed, freqs=freqs, usedepletion=usedepletion)
    temp.Run_simulation()
    temp.Calculate_gaussian_fit_parameters()
    return([temp.fit_info[0], 1-temp.fit_info[1]])

def derivative(x0= [], usedepletion=False, dx=.1, trials=1000):
    return [((-1*calc_mean_variance_offset_squared([x0[j] if i != j else x0[j]-(dx/2) for j in range(len(x0))], trials=trials) + calc_mean_variance_offset_squared([x0[j] if i != j else x0[j]+(dx/2) for j in range(len(x0))], trials=trials))/dx) for i in range(len(x0))]

def plot_fit_distro(freqs=[-4,-3,-2,-1,0,1,2,3,4], trials=1000, figtitle="test"):
    pool = mp.Pool(mp.cpu_count())
    fit_info=list(tqdm(pool.imap_unordered(partial(single_run_fit, freqs=freqs, usedepletion=False), range(trials)), total=trials))
    pool.close()
    pool.join()
    fit_info=np.array(fit_info)
    plt.hist2d(fit_info[:,0], fit_info[:,1], range=[[-0.5, 0.5],[-0.5, 0.5]], bins=100, cmap=plt.cm.BuPu)
    plt.xlabel("Fit Mean")
    plt.ylabel("Fit Variance - 1")
    plt.title("Distribution of fit parameters {}".format(figtitle))
    textstr = '\n'.join((r'$\mu=%.4f$' % (np.mean(fit_info[:,0]) ),r'$\sigma=%.4f$' % (np.mean(fit_info[:,1]))))
    plt.text(-0.45, 0.4, textstr)
    plt.savefig("Data/{}".format(figtitle)+".png")
    plt.close()

if __name__=="__main__":
    # res= minimize(calc_mean_variance_offset_squared, x0=[-2,-1.5,-.5,0,1.5,1,2], args=200, options={'disp':True}, method='Nelder-Mead')
    # print(res)
    plot_fit_distro(trials=50, freqs=[-1.5,-1,0,1,1.5], figtitle="initial")
    plot_fit_distro(trials=50, freqs=[ -1.60763833e+00,  -1.04378550e+00,  -3.10785582e-04, 1.00834690e+00,   1.58763405e+00], figtitle="final")